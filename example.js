var cars = [
    {
        model: 'Mazda',
        year: 1998,
        power: 244
    },
    {
        model: 'Mercedes',
        year: 2004,
        power: 138
    },
];

var isServiceFull=true;

function getCarsNames(cars){
    var prefix ='CS';

    return cars.map(function(car) {
        return prefix + car.model;
    });
} 

function checkServiceStatus() {
    if (isServiceFull) {
        console.log('Service is full, no more place for new cars');
    }else {
        console.log('We still have a place for new car');
    }
}

getCarsNames(cars);
checkServiceStatus();