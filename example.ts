export class CarsService {
    constructor(){ 
    this.getCarsNames(this.cars);
    this.checkServiceStatus();
    }
    cars : Car[] = [
        {
            model: 'Mazda',
            year: 1998,
            power: 244
        },
        {
            model: 'Mercedes',
            year: 2004,
            power: 138
        },
    ];

    isServiceFull : boolean=true;

    getCarsNames(cars : Car[]) : string[]{// :string mówi nam co zwraca funkcja
        const prefix : string ='CS';
    
        return cars.map((car : Car) => {
            return prefix + car.model;
        });
    } 
    
    checkServiceStatus() : void {//jak metoda nic nie zwraca to zostaje void
        if (this.isServiceFull) {
            console.log('Service is full, no more place for new cars');
        }else {
            console.log('We still have a place for new car');
        }
    }
}

export interface Car { //interfejs pokazuje z jakich typów składa się obiekt
    model: string;
    year: number;
    power: number;
}